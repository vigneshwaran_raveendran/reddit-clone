import unittest

from application.sorted_set.sorted_set import SortedSet


class TestSortedSet(unittest.TestCase):
    def setUp(self):
        self.sorted_set = SortedSet()

    def test_empty_sorted_set(self):
        self.assertEqual(0, len(self.sorted_set))

    def test_get_non_existent_key_returns_None(self):
        rank = self.sorted_set.get('NonExistentKey')
        self.assertIsNone(rank)

    def test_put_adds_new_elements_with_its_rank(self):
        self.sorted_set.put("Hello", 2)
        self.sorted_set.put("World", 3)
        self.sorted_set.put(":)", 2)
        self.assertEqual(3, len(self.sorted_set))
        self.assertEqual(2, self.sorted_set.get("Hello"))
        self.assertEqual(3, self.sorted_set.get("World"))
        self.assertEqual(2, self.sorted_set.get(":)"))

    def test_put_updates_an_existing_element_with_new_rank(self):

        for i in range(10):
            self.sorted_set.put("Hello", i)
            self.assertEqual(1, len(self.sorted_set))
            self.assertEqual(i, self.sorted_set.get("Hello"))

    def test_get_items_without_limit_returns_all_items_in_descending_order(self):
        list_of_tuples = []

        for i in range(10):
            key = "Key_{}".format(i)
            self.sorted_set.put(key, i)
            list_of_tuples.append((key, i))

        result = self.sorted_set.get_items()
        expected_result = reversed(list_of_tuples)
        self.assertCountEqual(expected_result, result)


    def test_get_items_with_limit_returns_items_in_descending_order_only_upto_specified_limit(self):
        list_of_tuples = []

        for i in range(10):
            key = "Key_{}".format(i)
            self.sorted_set.put(key, i)
            list_of_tuples.append((key, i))

        result = self.sorted_set.get_items(5)
        reversed_list_of_tuples = list(reversed(list_of_tuples))
        expected_result = reversed_list_of_tuples[:5]
        self.assertCountEqual(expected_result, result)


    def test_increment_puts_the_key_with_1_rank_if_key_not_present_and_rank_not_given(self):
        self.assertEqual(0, len(self.sorted_set))
        new_rank = self.sorted_set.increment("key")
        self.assertEqual(1, new_rank)
        self.assertEqual(1, len(self.sorted_set))
        self.assertEqual(1, self.sorted_set.get("key"))


    def test_increment_puts_the_key_with_specified_rank_if_key_not_present_and_rank_given(self):
        self.assertEqual(0, len(self.sorted_set))
        new_rank = self.sorted_set.increment("key", 5)
        self.assertEqual(5, new_rank)
        self.assertEqual(1, len(self.sorted_set))
        self.assertEqual(5, self.sorted_set.get("key"))


    def test_increment_increments_the_rank_of_existing_key_by_1_if_rank_not_given(self):
        self.sorted_set.put("key", 1)
        self.assertEqual(1, len(self.sorted_set))
        new_rank = self.sorted_set.increment("key")
        self.assertEqual(2, new_rank)
        self.assertEqual(1, len(self.sorted_set))
        self.assertEqual(2, self.sorted_set.get("key"))

    def test_increment_increments_the_rank_of_existing_key_by_specified_rank_if_rank_given(self):
        self.sorted_set.put("key", 1)
        self.assertEqual(1, len(self.sorted_set))
        new_rank = self.sorted_set.increment("key", -1)
        self.assertEqual(0, new_rank)
        self.assertEqual(1, len(self.sorted_set))
        self.assertEqual(0, self.sorted_set.get("key"))