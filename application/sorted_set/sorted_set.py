from collections import OrderedDict
from threading import RLock

from sortedcontainers import SortedDict


class SortedSet:
    '''
    This datastructure is similar to the Redis SortedSet in functionality
    You can use this like a dictionary and the values are ordered in descending order (by default)

    Using re-entrant locks in every method for consistency between the __key_to_rank_map and __rank_to_keys_map

    The __key_to_rank_map is a simple dictionary that maps the keys to their ranks

    The __rank_to_keys_map is a SortedDict where key is the rank and the value is an OrderedDict of original keys for
    this rank.

    The reason for using OrderedDict over a normal set is to preserve the insertion order so iteration is in predictable
    order

    The idea for this DS comes from this article which about how you can implement something like Redis' SortedSet
    on your own. See the 'Internals' section
    https://scalegrid.io/blog/introduction-to-redis-data-structures-sorted-sets/

    Limitations:
    * Because this object lives in process memory, you must serve this web app in single process mode
    '''

    def __init__(self, ordering_func=lambda x: -x):
        self.lock = RLock()
        with self.lock:
            self.__key_to_rank_map = {}
            self.__rank_to_keys_map = SortedDict(ordering_func) # sort in reverse order

    def __len__(self):
        with self.lock:
            return len(self.__key_to_rank_map)


    def get(self, key):
        '''
        Get the rank for the associated key
        :return: the rank if key present; None otherwise
        '''
        with self.lock:
            return self.__key_to_rank_map.get(key)

    def put(self, key, rank):
        '''
        Updates the key with appropriate rank
        '''
        with self.lock:
            existing_rank = self.get(key)

            if existing_rank is None:
                self.__add_new_entry(key, rank)
            else:
                self.__update_existing_entry(key, existing_rank, rank)



    def get_items(self, limit=None):
        '''
        Returns the entries as a list of tuple in defined order

        For eg. [ ('Value2', 1000), ('Value1', 500), ('Value3', 200) ]
        :param limit: Caps by this limit. If limit is None, returns all the entries
        '''
        with self.lock:
            result = []
            counter = 0
            for rank, keys in self.__rank_to_keys_map.items():
                for key in keys.keys():
                    result.append((key, rank))
                    counter += 1
                    if limit is not None and counter == limit: # if limit is reached, break and return the list
                        return result
            return result

    def increment(self, key, rank=1):
        '''
        Increments the rank of the key by the specified rank (positive or negative)

        If the key is not found, it sets the key with the specified rank
        :return: the new rank
        '''
        with self.lock:
            existing_rank = self.get(key)

            if existing_rank is None:
                self.__add_new_entry(key, rank)
                return rank
            else:
                self.__update_existing_entry(key, existing_rank, existing_rank + rank)
                return existing_rank + rank


    def __add_new_entry(self, key, rank):
        '''
        Adds a new entry to __key_to_rank_map and __rank_to_keys_map
        '''
        with self.lock:
            self.__key_to_rank_map[key] = rank
            self.__add_key_for_rank(key, rank)

    def __update_existing_entry(self, key, existing_rank, new_rank):
        '''
        Updates the existing entry in __key_to_rank_map and reassigns the ranking in __rank_to_keys_map
        '''
        with self.lock:
            self.__remove_key_for_rank(key, existing_rank)
            self.__key_to_rank_map[key] = new_rank
            self.__add_key_for_rank(key, new_rank)



    def __add_key_for_rank(self, key, rank):
        '''
        Looks for the rank in __rank_to_keys_map and adds the key argument as one of the values for the rank
        '''
        with self.lock:
            keys_set = self.__rank_to_keys_map.get(rank)
            if keys_set is None:
                self.__rank_to_keys_map[rank] = OrderedDict({key: 1})
            else:
                keys_set[key] = 1

    def __remove_key_for_rank(self, key, existing_rank):
        '''
        Looks for the rank in __rank_to_keys_map and removes the key from its values
        If there are no more values for this rank, then the entry is removed as well
        '''
        with self.lock:
            keys_set = self.__rank_to_keys_map.get(existing_rank)
            del keys_set[key]
            if len(keys_set) == 0:
                del self.__rank_to_keys_map[existing_rank]


