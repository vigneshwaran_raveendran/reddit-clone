import logging.config

import os
from flask import Flask, Blueprint

from application import settings
from application.api.restplus import api
from application.api.endpoints.topics import ns as topics_namespace


logging_conf_path = os.path.normpath(os.path.join(os.path.dirname(__file__), '../logging.conf'))
logging.config.fileConfig(logging_conf_path)
log = logging.getLogger(__name__)

def configure_app(flask_app):
    flask_app.config['SWAGGER_UI_DOC_EXPANSION'] = settings.RESTPLUS_SWAGGER_UI_DOC_EXPANSION
    flask_app.config['RESTPLUS_VALIDATE'] = settings.RESTPLUS_VALIDATE
    flask_app.config['RESTPLUS_MASK_SWAGGER'] = settings.RESTPLUS_MASK_SWAGGER
    flask_app.config['ERROR_404_HELP'] = settings.RESTPLUS_ERROR_404_HELP



def create_app():
    app = Flask(__name__)
    configure_app(app)

    blueprint = Blueprint('api', __name__, url_prefix='/api')
    api.init_app(blueprint)
    api.add_namespace(topics_namespace)
    app.register_blueprint(blueprint)
    return app


def main():
    app = create_app()
    app.run(debug=settings.FLASK_DEBUG)


if __name__ == "__main__":
    main()
