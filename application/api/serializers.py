from flask_restplus import fields

from application.api.restplus import api

topic = api.model('Topic', {
    'topic': fields.String(required=True, max_length=255, min_length=1, description='Topic string')
})

topic_with_votes = api.inherit('Topic with votes', topic, {
    'votes': fields.Integer(readOnly=True, description='Number of votes for this topic')
})


