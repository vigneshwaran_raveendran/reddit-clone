import logging

from application.sorted_set.sorted_set import SortedSet

log = logging.getLogger(__name__)

class Model:
    """
    This class follows singleton pattern.
    """
    __instance = None

    def __new__(cls):
        """
        :return: The singleton instance of this class.
        """
        if Model.__instance is None:
            log.info("Creating new instance of Model")
            Model.__instance = object.__new__(cls)
            Model.__initialize(Model.__instance)
            return Model.__instance
        else:
            raise RuntimeError("Singleton instance already exists: %s" % cls)

    @classmethod
    def get_instance(cls):
        """
        Get the singleton instance of this class. This can only be called after the instance is properly initialized.
        """
        if cls.__instance is None:
            Model()
        return cls.__instance

    @staticmethod
    def __initialize(instance):
        try:
            instance.sorted_set = SortedSet()
        except Exception as e:
            log.exception('Error instantiating Model: {}'.format(e))

    @classmethod
    def get_topics(cls, limit=None):
        """
        :return: an ordered list of topics with their votes
        """
        topic_list = cls.__instance.sorted_set.get_items(limit)
        response = [ {'topic': topic, 'votes': votes} for topic, votes in topic_list ]
        return response


    @classmethod
    def get_topic(cls, topic):
        """
        :return: the topic with their votes
        """
        votes = cls.__instance.sorted_set.get(topic)
        if votes is not None:
            return {'topic': topic, 'votes': votes}
        return None

    @classmethod
    def create_topic(cls, topic):
        cls.__instance.sorted_set.put(topic, 0)
        return None

    @classmethod
    def upvote_topic(cls, topic):
        current_votes = cls.__instance.sorted_set.increment(topic)
        return {'topic': topic, 'votes': current_votes}

    @classmethod
    def downvote_topic(cls, topic):
        current_votes = cls.__instance.sorted_set.increment(topic, -1)
        return {'topic': topic, 'votes': current_votes}
