import unittest

from application.api.model import Model
from application.app import create_app

class TopicsNamespaceTest(unittest.TestCase):

    def setUp(self):
        self.app = create_app()
        self.client = self.app.test_client

    def tearDown(self):
        Model._Model__instance = None


class TopicsCollectionTest(TopicsNamespaceTest):

    def test_get_returns_200_for_blank_slate(self):
        res = self.client().get('/api/topics/')
        self.assertEqual(res.status_code, 200)
        self.assertCountEqual([], res.json)

    def test_get_limits_the_number_of_topics_returned_as_per_qparam(self):
        for i in range(20):
            json = {
                "topic": "{}".format(i)
            }
            self.client().post('/api/topics/', json=json)

        res = self.client().get('/api/topics/?limit=5')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(5, len(res.json))
        self.assertDictEqual({
            "topic": "0",
            "votes": 0
        }, res.json[0])


    def test_get_limits_the_number_of_topics_by_10_if_not_explicitly_specified(self):
        for i in range(20):
            json = {
                "topic": "{}".format(i)
            }
            self.client().post('/api/topics/', json=json)

        res = self.client().get('/api/topics/')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(10, len(res.json))
        self.assertDictEqual({
            "topic": "0",
            "votes": 0
        }, res.json[0])


    def test_post_returns_201_after_creating_the_given_topic(self):
        json = {
            "topic": "abc"
        }
        res = self.client().post('/api/topics/', json=json)
        self.assertEqual(res.status_code, 201)
        res = self.client().get('/api/topics/')
        self.assertEqual(res.status_code, 200)
        self.assertDictEqual({
            "topic": "abc",
            "votes": 0
        }, res.json[0])

    def test_post_returns_400_if_topic_already_exists(self):
        json = {
            "topic": "a"*255
        }
        res = self.client().post('/api/topics/', json=json)
        self.assertEqual(res.status_code, 201)
        res = self.client().post('/api/topics/', json=json)
        self.assertEqual(res.status_code, 400)
        self.assertDictEqual({
            "message": "This topic is already existing"
        }, res.json)


    def test_post_returns_400_if_no_data_given(self):
        res = self.client().post('/api/topics/')
        self.assertEqual(res.status_code, 400)
        self.assertEqual("Input payload validation failed", res.json['message'])


    def test_post_returns_400_if_topic_not_matching_min_length(self):
        json = {
            "topic": ""
        }
        res = self.client().post('/api/topics/', json=json)
        self.assertEqual(res.status_code, 400)
        self.assertEqual("Input payload validation failed", res.json['message'])


    def test_post_returns_400_if_topic_not_matching_max_length(self):
        json = {
            "topic": 'a'*256
        }
        res = self.client().post('/api/topics/', json=json)
        self.assertEqual(res.status_code, 400)
        self.assertEqual("Input payload validation failed", res.json['message'])


class UpvoteTopicItemTest(TopicsNamespaceTest):

    def test_post_returns_400_if_topic_not_found(self):
        json = {
            "topic": "non-existing"
        }
        res = self.client().post('/api/topics/upvote', json=json)
        self.assertEqual(res.status_code, 400)
        self.assertDictEqual({
            "message": "This topic is not found"
        }, res.json)

    def test_post_returns_200_with_topic_and_latest_votes_if_topic_found(self):
        json = {
            "topic": "Hello"
        }
        res = self.client().post('/api/topics/', json=json)
        self.assertEqual(res.status_code, 201)

        res = self.client().post('/api/topics/upvote', json=json)
        self.assertEqual(res.status_code, 200)
        self.assertDictEqual({
            "topic": "Hello",
            "votes": 1
        }, res.json)


    def test_multiple_upvotes(self):
        for i in range(10):
            json = {
                "topic": "Hello_{}".format(i)
            }
            self.client().post('/api/topics/', json=json)
        for i in range(5):
            self.client().post('/api/topics/upvote', json={
                "topic": "Hello_5"
            })
        res = self.client().get('/api/topics/?limit=5')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(5, len(res.json))
        self.assertDictEqual({
            "topic": "Hello_5",
            "votes": 5
        }, res.json[0])

class DownvoteTopicItemTest(TopicsNamespaceTest):

    def test_post_returns_400_if_topic_not_found(self):
        json = {
            "topic": "non-existing"
        }
        res = self.client().post('/api/topics/downvote', json=json)
        self.assertEqual(res.status_code, 400)
        self.assertDictEqual({
            "message": "This topic is not found"
        }, res.json)

    def test_post_returns_200_with_topic_and_latest_votes_if_topic_found(self):
        json = {
            "topic": "Hello"
        }
        res = self.client().post('/api/topics/', json=json)
        self.assertEqual(res.status_code, 201)

        for i in range(11):
            self.client().post('/api/topics/upvote', json=json)

        res = self.client().post('/api/topics/downvote', json=json)
        self.assertEqual(res.status_code, 200)
        self.assertDictEqual({
            "topic": "Hello",
            "votes": 10
        }, res.json)


