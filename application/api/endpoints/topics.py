import logging

from flask import request
from flask_restplus import Resource
from werkzeug.exceptions import BadRequest

from application.api.model import Model
from application.api.parsers import limit_args
from application.api.restplus import api
from application.api.serializers import topic, topic_with_votes

log = logging.getLogger(__name__)

ns = api.namespace('topics', description='Operations related to topics')

@ns.route('/')
class TopicsCollection(Resource):

    @api.expect(limit_args)
    @api.marshal_list_with(topic_with_votes)
    def get(self):
        """
        Returns list of topics and their votes.
        """
        args = limit_args.parse_args(request)
        limit = args.get('limit', 10)
        topics = Model.get_instance().get_topics(limit)
        return topics

    @api.expect(topic)
    def post(self):
        """
        Creates a new topic with 0 vote.
        """
        data = request.json
        topic = data['topic']
        existing_topic = Model.get_instance().get_topic(topic)
        if existing_topic is not None:
            raise BadRequest('This topic is already existing')
        Model.get_instance().create_topic(topic)
        return '', 201


@ns.route('/upvote')
class UpvoteTopicItem(Resource):

    @api.expect(topic)
    def post(self):
        """
        Upvotes the specified topic by 1 vote.
        """
        data = request.json
        topic = data['topic']
        existing_topic = Model.get_instance().get_topic(topic)
        if not existing_topic:
            raise BadRequest('This topic is not found')
        response = Model.get_instance().upvote_topic(topic)
        return response, 200

@ns.route('/downvote')
class DownvoteTopicItem(Resource):

    @api.expect(topic)
    def post(self):
        """
        Downvotes the specified topic by 1 vote.
        """
        data = request.json
        topic = data['topic']
        existing_topic = Model.get_instance().get_topic(topic)
        if not existing_topic:
            raise BadRequest('This topic is not found')
        response = Model.get_instance().downvote_topic(topic)
        return response, 200
