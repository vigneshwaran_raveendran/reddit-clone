import unittest
from unittest.mock import patch

from application.api.model import Model


class TestModel(unittest.TestCase):
    def setUp(self):
        self.model = Model.get_instance()

    def tearDown(self):
        Model._Model__instance = None

    def test_get_singleton_instance(self):
        self.assertTrue(self.model is Model.get_instance())

    @patch('application.sorted_set.sorted_set.SortedSet.__init__')
    def test_initialise_should_create_an_instance_of_sorted_set(self, mock_sorted_set_init):
        mock_sorted_set_init.return_value = None
        Model._Model__instance = None
        Model.get_instance()
        mock_sorted_set_init.assert_called_with()


    @patch('application.sorted_set.sorted_set.SortedSet.get_items')
    def test_get_topics_without_limit_should_return_all_topics(self, mock_sorted_set_get_items):
        mock_sorted_set_get_items.return_value = [('Topic2', 1000), ('Topic1', 500)]
        expected_response = [{
            'topic': 'Topic2',
            'votes': 1000
        }, {
            'topic': 'Topic1',
            'votes': 500
        }]
        result = self.model.get_topics()
        mock_sorted_set_get_items.assert_called_with(None)
        self.assertCountEqual(expected_response, result)


    @patch('application.sorted_set.sorted_set.SortedSet.get_items')
    def test_get_topics_with_limit_should_return_topics_upto_that_limit(self, mock_sorted_set_get_items):
        mock_sorted_set_get_items.return_value = [('Topic2', 1000)]
        expected_response = [{
            'topic': 'Topic2',
            'votes': 1000
        }]
        result = self.model.get_topics(limit=1)
        mock_sorted_set_get_items.assert_called_with(1)
        self.assertCountEqual(expected_response, result)


    @patch('application.sorted_set.sorted_set.SortedSet.get')
    def test_get_topic_should_return_None_if_not_present(self, mock_sorted_set_get):
        mock_sorted_set_get.return_value = None
        result = self.model.get_topic('Topic2')
        mock_sorted_set_get.assert_called_with('Topic2')
        self.assertEqual(None, result)

    @patch('application.sorted_set.sorted_set.SortedSet.get')
    def test_get_topic_should_return_the_topic_with_its_votes(self, mock_sorted_set_get):
        mock_sorted_set_get.return_value = 1000
        expected_response = {
            'topic': 'Topic2',
            'votes': 1000
        }
        result = self.model.get_topic('Topic2')
        mock_sorted_set_get.assert_called_with('Topic2')
        self.assertDictEqual(expected_response, result)

    @patch('application.sorted_set.sorted_set.SortedSet.put')
    def test_create_topic_should_create_the_topic_with_0_votes(self, mock_sorted_set_put):
        mock_sorted_set_put.return_value = None
        self.model.create_topic('Topic2')
        mock_sorted_set_put.assert_called_with('Topic2', 0)

    @patch('application.sorted_set.sorted_set.SortedSet.increment')
    def test_upvote_topic_should_increment_the_rank_of_the_topic_by_1_vote(self, mock_sorted_set_increment):
        mock_sorted_set_increment.return_value = 1
        expected_response = {
            'topic': 'Topic2',
            'votes': 1
        }
        response = self.model.upvote_topic('Topic2')
        mock_sorted_set_increment.assert_called_with('Topic2')
        self.assertDictEqual(response, expected_response)

    @patch('application.sorted_set.sorted_set.SortedSet.increment')
    def test_downvote_topic_should_decrement_the_rank_of_the_topic_by_1_vote(self, mock_sorted_set_decrement):
        mock_sorted_set_decrement.return_value = -1
        expected_response = {
            'topic': 'Topic2',
            'votes': -1
        }
        response = self.model.downvote_topic('Topic2')
        mock_sorted_set_decrement.assert_called_with('Topic2', -1)
        self.assertDictEqual(response, expected_response)


