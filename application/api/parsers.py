from flask_restplus import reqparse

limit_args = reqparse.RequestParser()
limit_args.add_argument('limit', type=int, required=False, default=10, help='No of topics to show')
