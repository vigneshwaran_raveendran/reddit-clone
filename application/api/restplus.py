import logging

from flask_restplus import Api
from application import settings

log = logging.getLogger(__name__)

api = Api(version='1.0', title='My Reddit Clone API',
          description='Reddit Clone - where you can post topics or upvote/downvote any topics')


@api.errorhandler
def default_error_handler(e):
    message = 'An unhandled exception occurred.'
    log.exception(message)

    if not settings.FLASK_DEBUG:
        return {'message': message}, 500