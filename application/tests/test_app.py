import unittest

from application.app import create_app


class TestApp(unittest.TestCase):
    """Testing that the app is bootstrapped properly"""


    def setUp(self):
        self.app = create_app()
        self.client = self.app.test_client

    def test_root_url_returns_400(self):
        res = self.client().get('/')
        self.assertEqual(res.status_code, 404)

    def test_api_endpoint_returns_200(self):
        res = self.client().get('/api/')
        self.assertEqual(res.status_code, 200)

    def test_swaggerui_running_properly(self):
        res = self.client().get('/api/swagger.json')
        self.assertEqual(res.status_code, 200)