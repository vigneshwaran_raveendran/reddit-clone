## Vignesh's Reddit Clone

**Online demo**

https://vignesh-reddit-clone.herokuapp.com/api



**How to run locally**

Execute these commands from the project root

```
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
PYTHONPATH=. gunicorn "application.app:create_app()"
```


**How I approached this problem**

* I looked at Reddit website and upvoted a topic and looked at the request it was making.
Personally I didn't feel they are using proper REST urls so I came up with my own.
* I looked for online hosts but ended up with Heroku as I had tried that before.
* I chose Python as I'm working on a Django project now.
* I chose Flask as it is lightweight and suitable for the given requirements
* I chose FlaskRESTPlus as the rest framework as it helps generate the Swagger UI automatically which saves me time
* I looked for a boilerplate for making a RESTful app in Flask and created the layout referring from it.
* I implemented this in proper TDD fashion as you can see from `git log`
* Additionally, I also lifted [my own Guard boilerplate](https://github.com/vigneshwaranr/SampleAutoGuardTesting#how-to-run-this-project)
 that helps run the tests automatically whenever there is a change in python code. Just a couple of files -> `Gemfile` and a `Guardfile`
* Every commit was pushed and verified in Heroku also
* I implemented the DataStructure on my own by putting together other DS like SortedDict and OrderedDict. Please see the docstring of the sorted_set.py
* Comments can be found inline in the code and the tests can act as the specification.

**Known Limitations**

* Because the DS lives in process memory as per requirement, we should configure the web server to run in single process mode.
* But in Heroku, even though I set only one web dyno to serve the requests, I suspect that if I send requests very fast, it seems to spawn more processes.
  So you get inconsistent results. This does not happen if you execute the application locally in your machine.


**References:**

* Learnt how to deploy flask app to heroku from this - https://progblog.io/How-to-deploy-a-Flask-App-to-Heroku/
* Learnt how to do TDD in flask from this - https://scotch.io/tutorials/build-a-restful-api-with-flask-the-tdd-way
* Boilerplate I lifted from - http://michal.karzynski.pl/blog/2016/06/19/building-beautiful-restful-apis-using-flask-swagger-ui-flask-restplus/
